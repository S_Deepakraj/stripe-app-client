import React, {useContext} from "react";
import { Link } from 'react-router-dom'
import { useNavigate } from "react-router-dom";
import { UserContext } from "../context";

function Navbar() {
  const [state, setState] = useContext(UserContext)
  const navigate = useNavigate()
  const logout = () => {
    localStorage.removeItem('auth')
    setState({
      token: "",
      user: {},
    });
    navigate('/login')
  }
  console.log('state from nav',state)
  return (
    <nav className="navbar navbar-expand-lg navbar-light bg-light">
      <ul className="navbar-nav">
        <li className="nav-item">
          <Link className="nav-link" to="/">
            Home
          </Link>
        </li>
        {state && state.token ? (
          <li className="nav-item">
            <span onClick={logout} className="nav-link" >
              Logout
            </span>
          </li>
        ) : (
          <>
            <li className="nav-item">
              <Link className="nav-link" to="/register">
                Sign up
              </Link>
            </li>
            <li className="nav-item">
              <Link className="nav-link" to="/login">
                Login
              </Link>
            </li>
          </>
        )}
      </ul>
    </nav>
  );
}

export default Navbar;
