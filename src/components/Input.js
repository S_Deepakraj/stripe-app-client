import React from "react";

function Input({ label, value, setValue, type = "text" }) {
  return (
    <div className="input-group mb-3">
      <div className="input-group-prepend">
        <span className="input-group-text" id="basic-addon1">
         {label}
        </span>
      </div>
      <input
        type={type}
        className="form-control"
        value={value}
        onChange={(event) => setValue(event.target.value)}
      />
    </div>
  );
}

export default Input;
