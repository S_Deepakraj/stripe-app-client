import React from "react";

function Button({ type="primary", text = "Submit", size, handleClick }) {
  return (
    <button
      onClick={handleClick}
      type="button"
      className={`btn btn-${type} btn-${size}`}
    >
      {text}
    </button>
  );
}

export default Button;
