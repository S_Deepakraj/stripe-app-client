import React, { useState, useContext } from "react";
import axios from "axios";
import toast from "react-hot-toast";
import Input from "../components/Input";
import Button from "../components/Button";
import { UserContext } from "../context";
import { useNavigate } from "react-router-dom";

function Login() {
  const [state, setState] = useContext(UserContext)
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");

  const navigate = useNavigate();

  const handleClick = async (e) => {
    try {
      e.preventDefault();
      const { data } = await axios.post("http://localhost:8000/api/login", {
        email,
        password,
      });
      if (data.error) {
        toast.error(data.error);
      } else {
        setEmail("");
        setPassword("");
        localStorage.setItem('auth', JSON.stringify(data))
        setState(data)
        toast.success(
          `Congrats ${data.user.name}, You are logged in successfully!`
        );
        navigate("/secret");
      }
    } catch (err) {
      console.log(err);
      toast.error("Ooops... Something went wrong!");
    }
  };
  return (
    <div className="main">
      <div>
        <h1>Login</h1>
      </div>
      <div className="form-group">
        <Input label="Email" value={email} setValue={setEmail} type="email" />
        <Input
          label="Password"
          value={password}
          setValue={setPassword}
          type="password"
        />
        <div className="d-grid">
          <Button
            text="Submit"
            type="primary"
            size="sm"
            handleClick={handleClick}
          />
        </div>
      </div>
    </div>
  );
}

export default Login;



