import React, { useState } from "react";
import axios from "axios";
import toast from "react-hot-toast";
import Input from "../components/Input";
import Button from "../components/Button";

import {useNavigate} from 'react-router-dom'

function Register() {
  const [name, setName] = useState("");
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");

  const navigate = useNavigate()

  const handleClick = async (e) => {
    try {
      e.preventDefault();
      const { data } = await axios.post("http://localhost:8000/api/register", {
        name,
        email,
        password,
      });
      if (data.error) {
        toast.error(data.error);
      } else {
        setName("")
        setEmail("")
        setPassword("")
        toast.success(`Congrats ${data.user.name}, You are signed up successfully!`);
        navigate('/login')
      }
    } catch (err) {
      console.log(err);
      toast.error("Ooops... Something went wrong!");
    }
  };
  return (
    <div className="main">
      <div>
        <h1>Sign up</h1>
      </div>
      <div className="form-group">
        <Input label="Name" value={name} setValue={setName} />
        <Input label="Email" value={email} setValue={setEmail} type="email" />
        <Input
          label="Password"
          value={password}
          setValue={setPassword}
          type="password"
        />
        <div className="d-grid">
          <Button
            text="Submit"
            type="primary"
            size="sm"
            handleClick={handleClick}
          />
        </div>
      </div>
    </div>
  );
}

export default Register;
