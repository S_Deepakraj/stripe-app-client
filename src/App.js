import React from 'react';
import "./App.css"
import {BrowserRouter as Router, Routes, Route} from 'react-router-dom'
import { Toaster } from "react-hot-toast";
import Home from './pages/Home';
import Register from './pages/Register';
import Login from './pages/Login';
import Secret from './pages/Secret';
import Navbar from './components/Navbar';

function App() {
  return (
    <Router>
      <Navbar />
      <Toaster position="top-center" />
      <Routes>
        <Route exact path="/" Component={Home} />
        <Route exact path="/login" Component={Login} />
        <Route exact path="/register" Component={Register} />
        <Route exact path="/secret" Component={Secret} />
      </Routes>
    </Router>
  );
}

export default App;
